package com.mehmet6parmak.training.fragments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mehmet6parmak.training.fragments.fragments.CommunicationContentFragment;
import com.mehmet6parmak.training.fragments.fragments.CommunicationTitlesFragment;

public class CommunicationActivity extends AppCompatActivity implements CommunicationTitlesFragment.OnTitleSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication);
    }

    @Override
    public void onTitleSelected(String title, String uri) {


        getSupportActionBar().setTitle(title);

        CommunicationContentFragment fragment = (CommunicationContentFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_content);
        fragment.loadUrl(uri);
    }
}
