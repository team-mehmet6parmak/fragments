package com.mehmet6parmak.training.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.mehmet6parmak.training.fragments.fragments.WebViewFragment;

public class FragmentWithParametersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_with_parameters);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.layout_container, WebViewFragment.newInstance("https://google.com"), "google").commit();
        }
    }
}
