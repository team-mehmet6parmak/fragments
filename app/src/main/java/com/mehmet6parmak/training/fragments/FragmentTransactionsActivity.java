package com.mehmet6parmak.training.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.mehmet6parmak.training.fragments.fragments.ThirdFragment;

public class FragmentTransactionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_transactions);
    }


    public void onFragmentTransactionClicked(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment secondFragment = fragmentManager.findFragmentById(R.id.fragment_second);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        //transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        transaction.hide(secondFragment);
        transaction.add(R.id.layout_container, new ThirdFragment(), "ThirdFragment");

        //transaction.addToBackStack("some-transaction");

        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
