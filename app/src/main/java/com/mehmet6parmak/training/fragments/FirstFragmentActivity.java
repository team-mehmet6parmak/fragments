package com.mehmet6parmak.training.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.List;

public class FirstFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_fragment);
    }

    public void btnToggleFragmentClicked(View view) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        //get list of all fragments
        List<Fragment> fragments = fragmentManager.getFragments();
        //find fragments by id or tag
        //Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.first_fragment);
        Fragment fragment = fragmentManager.findFragmentByTag("FirstFragment");
        //start a new transaction
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        //revert the latest Fragment transaction
        fragmentManager.popBackStack();

        if (fragment.isVisible())
            transaction.hide(fragment);
        else
            transaction.show(fragment);
        transaction.commit();


    }
}
