package com.mehmet6parmak.training.fragments.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mehmet6parmak.training.fragments.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StateFragment extends Fragment implements View.OnClickListener {

    TextView tv;
    Button btn;

    public StateFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_state, container, false);

        tv = (TextView) view.findViewById(R.id.tv);
        view.findViewById(R.id.btn_increment).setOnClickListener(StateFragment.this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_increment) {
            int currentValue = Integer.valueOf(tv.getText().toString());
            tv.setText(String.valueOf(++currentValue));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("counter", tv.getText().toString());
    }
    //or

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            String counter = savedInstanceState.getString("counter");
            tv.setText(counter);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
