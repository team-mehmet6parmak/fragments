package com.mehmet6parmak.training.fragments.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mehmet6parmak.training.fragments.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment implements View.OnClickListener {

    TextView txtCounter;

    public FirstFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        view.findViewById(R.id.btn_increment).setOnClickListener(this);
        view.findViewById(R.id.btn_reset).setOnClickListener(this);

        txtCounter = (TextView) view.findViewById(R.id.txt_counter);

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_increment) {
            txtCounter.setText(String.valueOf(Integer.valueOf(txtCounter.getText().toString()) + 1));
        } else if (id == R.id.btn_reset) {
            txtCounter.setText("0");
        }
    }
}
