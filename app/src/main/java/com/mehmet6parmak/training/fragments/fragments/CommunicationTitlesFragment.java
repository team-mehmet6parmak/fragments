package com.mehmet6parmak.training.fragments.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mehmet6parmak.training.fragments.R;

public class CommunicationTitlesFragment extends Fragment implements View.OnClickListener {

    private OnTitleSelectedListener listener;

    public void setListener(OnTitleSelectedListener listener) {
        this.listener = listener;
    }

    String[] urls = new String[]{"https://developer.android.com/guide/topics/graphics/prop-animation.html",
            "https://developer.android.com/guide/topics/graphics/view-animation.html",
            "https://developer.android.com/guide/topics/graphics/drawable-animation.html"};

    String[] titles = new String[]{"Property Animation", "View Animation", "Drawable Animation"};

    public CommunicationTitlesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_communication_titles, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.tv_title).setOnClickListener(this);
        view.findViewById(R.id.tv_title_2).setOnClickListener(this);
        view.findViewById(R.id.tv_title_3).setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTitleSelectedListener) {
            listener = (OnTitleSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnTitleSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tv_title) {
            onTitleSelected(titles[0], urls[0]);
        } else if (id == R.id.tv_title_2) {
            onTitleSelected(titles[1], urls[1]);
        } else if (id == R.id.tv_title_3) {
            onTitleSelected(titles[2], urls[2]);
        }
    }

    protected void onTitleSelected(String title, String url) {
        if (listener != null) {
            listener.onTitleSelected(title, url);
        }
    }

    public interface OnTitleSelectedListener {
        void onTitleSelected(String title, String uri);
    }
}
