package com.mehmet6parmak.training.fragments.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.mehmet6parmak.training.fragments.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommunicationContentFragment extends Fragment {

    public CommunicationContentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_communication_content, container, false);
    }

    public void loadUrl(String uri) {
        WebView webView = (WebView) getView().findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(uri);
    }
}
