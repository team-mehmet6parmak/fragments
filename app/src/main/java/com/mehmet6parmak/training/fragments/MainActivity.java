package com.mehmet6parmak.training.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onFirstFragmentClicked(View view) {
        startActivity(new Intent(this, FirstFragmentActivity.class));
    }

    public void onFragmentTransactionClicked(View view) {
        startActivity(new Intent(this, FragmentTransactionsActivity.class));
    }

    public void onFragmentStateClicked(View view) {
        startActivity(new Intent(this, FragmentStateActivity.class));
    }

    public void onFragmentMenuClicked(View view) {
        startActivity(new Intent(this, FragmentMenuActivity.class));
    }

    public void onCommunicationClicked(View view) {
        startActivity(new Intent(this, CommunicationActivity.class));
    }

    public void onFragmentWithParametersClicked(View view) {
        startActivity(new Intent(this, FragmentWithParametersActivity.class));
    }
}
